// Generated by view binder compiler. Do not edit!
package com.example.rentapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.rentapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ListCarBinding implements ViewBinding {
  @NonNull
  private final CardView rootView;

  @NonNull
  public final CardView cardViewCar;

  @NonNull
  public final ImageView imageMobil;

  @NonNull
  public final TextView tvHarga;

  @NonNull
  public final TextView tvHargaSewaMobil;

  @NonNull
  public final TextView tvNamaMobil;

  private ListCarBinding(@NonNull CardView rootView, @NonNull CardView cardViewCar,
      @NonNull ImageView imageMobil, @NonNull TextView tvHarga, @NonNull TextView tvHargaSewaMobil,
      @NonNull TextView tvNamaMobil) {
    this.rootView = rootView;
    this.cardViewCar = cardViewCar;
    this.imageMobil = imageMobil;
    this.tvHarga = tvHarga;
    this.tvHargaSewaMobil = tvHargaSewaMobil;
    this.tvNamaMobil = tvNamaMobil;
  }

  @Override
  @NonNull
  public CardView getRoot() {
    return rootView;
  }

  @NonNull
  public static ListCarBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ListCarBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
      boolean attachToParent) {
    View root = inflater.inflate(R.layout.list_car, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ListCarBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      CardView cardViewCar = (CardView) rootView;

      id = R.id.imageMobil;
      ImageView imageMobil = ViewBindings.findChildViewById(rootView, id);
      if (imageMobil == null) {
        break missingId;
      }

      id = R.id.tvHarga;
      TextView tvHarga = ViewBindings.findChildViewById(rootView, id);
      if (tvHarga == null) {
        break missingId;
      }

      id = R.id.tvHargaSewaMobil;
      TextView tvHargaSewaMobil = ViewBindings.findChildViewById(rootView, id);
      if (tvHargaSewaMobil == null) {
        break missingId;
      }

      id = R.id.tvNamaMobil;
      TextView tvNamaMobil = ViewBindings.findChildViewById(rootView, id);
      if (tvNamaMobil == null) {
        break missingId;
      }

      return new ListCarBinding((CardView) rootView, cardViewCar, imageMobil, tvHarga,
          tvHargaSewaMobil, tvNamaMobil);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
