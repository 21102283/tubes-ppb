// Generated by view binder compiler. Do not edit!
package com.example.rentapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.rentapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ListMotorcycleBinding implements ViewBinding {
  @NonNull
  private final CardView rootView;

  @NonNull
  public final CardView cardViewMotor;

  @NonNull
  public final ImageView imageMotor;

  @NonNull
  public final TextView tvHarga;

  @NonNull
  public final TextView tvHargaSewaMotor;

  @NonNull
  public final TextView tvNamaMotor;

  private ListMotorcycleBinding(@NonNull CardView rootView, @NonNull CardView cardViewMotor,
      @NonNull ImageView imageMotor, @NonNull TextView tvHarga, @NonNull TextView tvHargaSewaMotor,
      @NonNull TextView tvNamaMotor) {
    this.rootView = rootView;
    this.cardViewMotor = cardViewMotor;
    this.imageMotor = imageMotor;
    this.tvHarga = tvHarga;
    this.tvHargaSewaMotor = tvHargaSewaMotor;
    this.tvNamaMotor = tvNamaMotor;
  }

  @Override
  @NonNull
  public CardView getRoot() {
    return rootView;
  }

  @NonNull
  public static ListMotorcycleBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ListMotorcycleBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.list_motorcycle, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ListMotorcycleBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      CardView cardViewMotor = (CardView) rootView;

      id = R.id.imageMotor;
      ImageView imageMotor = ViewBindings.findChildViewById(rootView, id);
      if (imageMotor == null) {
        break missingId;
      }

      id = R.id.tvHarga;
      TextView tvHarga = ViewBindings.findChildViewById(rootView, id);
      if (tvHarga == null) {
        break missingId;
      }

      id = R.id.tvHargaSewaMotor;
      TextView tvHargaSewaMotor = ViewBindings.findChildViewById(rootView, id);
      if (tvHargaSewaMotor == null) {
        break missingId;
      }

      id = R.id.tvNamaMotor;
      TextView tvNamaMotor = ViewBindings.findChildViewById(rootView, id);
      if (tvNamaMotor == null) {
        break missingId;
      }

      return new ListMotorcycleBinding((CardView) rootView, cardViewMotor, imageMotor, tvHarga,
          tvHargaSewaMotor, tvNamaMotor);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
