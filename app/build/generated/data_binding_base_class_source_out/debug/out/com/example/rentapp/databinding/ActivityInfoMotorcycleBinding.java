// Generated by view binder compiler. Do not edit!
package com.example.rentapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.rentapp.R;
import com.google.android.material.appbar.AppBarLayout;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityInfoMotorcycleBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final AppCompatButton btnSewaMotor;

  @NonNull
  public final RecyclerView recyclerView;

  @NonNull
  public final ScrollView scrollView;

  @NonNull
  public final RelativeLayout scrollViewLy;

  @NonNull
  public final Toolbar toolbar;

  @NonNull
  public final AppBarLayout toolbarLayout;

  @NonNull
  public final TextView tvToolbarWisata;

  private ActivityInfoMotorcycleBinding(@NonNull RelativeLayout rootView,
      @NonNull AppCompatButton btnSewaMotor, @NonNull RecyclerView recyclerView,
      @NonNull ScrollView scrollView, @NonNull RelativeLayout scrollViewLy,
      @NonNull Toolbar toolbar, @NonNull AppBarLayout toolbarLayout,
      @NonNull TextView tvToolbarWisata) {
    this.rootView = rootView;
    this.btnSewaMotor = btnSewaMotor;
    this.recyclerView = recyclerView;
    this.scrollView = scrollView;
    this.scrollViewLy = scrollViewLy;
    this.toolbar = toolbar;
    this.toolbarLayout = toolbarLayout;
    this.tvToolbarWisata = tvToolbarWisata;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityInfoMotorcycleBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityInfoMotorcycleBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_info_motorcycle, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityInfoMotorcycleBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnSewaMotor;
      AppCompatButton btnSewaMotor = ViewBindings.findChildViewById(rootView, id);
      if (btnSewaMotor == null) {
        break missingId;
      }

      id = R.id.recyclerView;
      RecyclerView recyclerView = ViewBindings.findChildViewById(rootView, id);
      if (recyclerView == null) {
        break missingId;
      }

      id = R.id.scrollView;
      ScrollView scrollView = ViewBindings.findChildViewById(rootView, id);
      if (scrollView == null) {
        break missingId;
      }

      id = R.id.scrollViewLy;
      RelativeLayout scrollViewLy = ViewBindings.findChildViewById(rootView, id);
      if (scrollViewLy == null) {
        break missingId;
      }

      id = R.id.toolbar;
      Toolbar toolbar = ViewBindings.findChildViewById(rootView, id);
      if (toolbar == null) {
        break missingId;
      }

      id = R.id.toolbarLayout;
      AppBarLayout toolbarLayout = ViewBindings.findChildViewById(rootView, id);
      if (toolbarLayout == null) {
        break missingId;
      }

      id = R.id.tvToolbarWisata;
      TextView tvToolbarWisata = ViewBindings.findChildViewById(rootView, id);
      if (tvToolbarWisata == null) {
        break missingId;
      }

      return new ActivityInfoMotorcycleBinding((RelativeLayout) rootView, btnSewaMotor,
          recyclerView, scrollView, scrollViewLy, toolbar, toolbarLayout, tvToolbarWisata);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
