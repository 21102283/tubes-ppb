// Generated by view binder compiler. Do not edit!
package com.example.rentapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.rentapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentHomeBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final RelativeLayout btnInfoMobil;

  @NonNull
  public final RelativeLayout btnInfoMotor;

  @NonNull
  public final LinearLayout compLayout1;

  @NonNull
  public final LinearLayout compLayout2;

  @NonNull
  public final RelativeLayout topBar;

  @NonNull
  public final TextView tvNameWelcome;

  @NonNull
  public final TextView tvWelcome;

  private FragmentHomeBinding(@NonNull RelativeLayout rootView,
      @NonNull RelativeLayout btnInfoMobil, @NonNull RelativeLayout btnInfoMotor,
      @NonNull LinearLayout compLayout1, @NonNull LinearLayout compLayout2,
      @NonNull RelativeLayout topBar, @NonNull TextView tvNameWelcome,
      @NonNull TextView tvWelcome) {
    this.rootView = rootView;
    this.btnInfoMobil = btnInfoMobil;
    this.btnInfoMotor = btnInfoMotor;
    this.compLayout1 = compLayout1;
    this.compLayout2 = compLayout2;
    this.topBar = topBar;
    this.tvNameWelcome = tvNameWelcome;
    this.tvWelcome = tvWelcome;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentHomeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentHomeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_home, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentHomeBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnInfoMobil;
      RelativeLayout btnInfoMobil = ViewBindings.findChildViewById(rootView, id);
      if (btnInfoMobil == null) {
        break missingId;
      }

      id = R.id.btnInfoMotor;
      RelativeLayout btnInfoMotor = ViewBindings.findChildViewById(rootView, id);
      if (btnInfoMotor == null) {
        break missingId;
      }

      id = R.id.compLayout1;
      LinearLayout compLayout1 = ViewBindings.findChildViewById(rootView, id);
      if (compLayout1 == null) {
        break missingId;
      }

      id = R.id.compLayout2;
      LinearLayout compLayout2 = ViewBindings.findChildViewById(rootView, id);
      if (compLayout2 == null) {
        break missingId;
      }

      id = R.id.topBar;
      RelativeLayout topBar = ViewBindings.findChildViewById(rootView, id);
      if (topBar == null) {
        break missingId;
      }

      id = R.id.tvNameWelcome;
      TextView tvNameWelcome = ViewBindings.findChildViewById(rootView, id);
      if (tvNameWelcome == null) {
        break missingId;
      }

      id = R.id.tvWelcome;
      TextView tvWelcome = ViewBindings.findChildViewById(rootView, id);
      if (tvWelcome == null) {
        break missingId;
      }

      return new FragmentHomeBinding((RelativeLayout) rootView, btnInfoMobil, btnInfoMotor,
          compLayout1, compLayout2, topBar, tvNameWelcome, tvWelcome);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
